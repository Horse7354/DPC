from abc import abstractmethod
import sys, os
import time
import math
import random
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),'../backbone'))
from select_backbone import select_resnet
from convrnn import ConvGRU


class DPC_RNN(nn.Module):
    '''DPC with RNN'''
    def __init__(self, sample_size, num_seq=8, seq_len=5, pred_step=3, network='resnet50'):
        super(DPC_RNN, self).__init__()
        torch.cuda.manual_seed(233)
        print('Using DPC-RNN model')
        self.sample_size = sample_size
        self.num_seq = num_seq
        self.seq_len = seq_len
        self.pred_step = pred_step
        self.last_duration = int(math.ceil(seq_len / 4))
        self.last_size = int(math.ceil(sample_size / 32))
        print('final feature map has size %dx%d' % (self.last_size, self.last_size))

        self.backbone, self.param = select_resnet(network, track_running_stats=False)
        self.param['num_layers'] = 1 # param for GRU
        self.param['hidden_size'] = self.param['feature_size'] # param for GRU

        self.agg = ConvGRU(input_size=self.param['feature_size'],
                               hidden_size=self.param['hidden_size'],
                               kernel_size=1,
                               num_layers=self.param['num_layers'])
        self.network_pred = nn.Sequential(
                                nn.Conv2d(self.param['feature_size'], self.param['feature_size'], kernel_size=1, padding=0),
                                nn.ReLU(inplace=True),
                                nn.Conv2d(self.param['feature_size'], self.param['feature_size'], kernel_size=1, padding=0)
                                )
        self.mask = None
        self.relu = nn.ReLU(inplace=False)
        self._initialize_weights(self.agg)
        self._initialize_weights(self.network_pred)

    def forward(self, block):
        # block: [B, N, C, SL, W, H]
        ### extract feature ###
        (B, N, C, SL, H, W) = block.shape
        block = block.view(B*N, C, SL, H, W)
        feature = self.backbone(block)
        del block
        feature = F.avg_pool3d(feature, (self.last_duration, 1, 1), stride=(1, 1, 1))

        feature_inf_all = feature.view(B, N, self.param['feature_size'], self.last_size, self.last_size) # before ReLU, (-inf, +inf)
        feature = self.relu(feature) # [0, +inf)
        feature = feature.view(B, N, self.param['feature_size'], self.last_size, self.last_size) # [B,N,D,6,6], [0, +inf)
        feature_inf = feature_inf_all[:, N-self.pred_step::, :].contiguous()
        del feature_inf_all

        ### aggregate, predict future ###
        _, hidden = self.agg(feature[:, 0:N-self.pred_step, :].contiguous())
        hidden = hidden[:,-1,:] # after tanh, (-1,1). get the hidden state of last layer, last time step
        
        pred = []
        for i in range(self.pred_step):
            # sequentially pred future
            p_tmp = self.network_pred(hidden)
            pred.append(p_tmp)
            _, hidden = self.agg(self.relu(p_tmp).unsqueeze(1), hidden.unsqueeze(0))
            hidden = hidden[:,-1,:]
        pred = torch.stack(pred, 1) # B, pred_step, xxx
        del hidden


        ### Get similarity score ###
        # pred: [B, pred_step, D, last_size, last_size]
        # GT: [B, N, D, last_size, last_size]
        N = self.pred_step
        # dot product D dimension in pred-GT pair, get a 6d tensor. First 3 dims are from pred, last 3 dims are from GT. 
        pred = pred.permute(0,1,3,4,2).contiguous().view(B*self.pred_step*self.last_size**2, self.param['feature_size'])
        feature_inf = feature_inf.permute(0,1,3,4,2).contiguous().view(B*N*self.last_size**2, self.param['feature_size']).transpose(0,1)
        score = torch.matmul(pred, feature_inf).view(B, self.pred_step, self.last_size**2, B, N, self.last_size**2)
        del feature_inf, pred

        if self.mask is None: # only compute mask once
            # mask meaning: -2: omit, -1: temporal neg (hard), 0: easy neg, 1: pos, -3: spatial neg
            mask = torch.zeros((B, self.pred_step, self.last_size**2, B, N, self.last_size**2), dtype=torch.int8, requires_grad=False).detach().cuda()
            mask[torch.arange(B), :, :, torch.arange(B), :, :] = -3 # spatial neg
            for k in range(B):
                mask[k, :, torch.arange(self.last_size**2), k, :, torch.arange(self.last_size**2)] = -1 # temporal neg
            tmp = mask.permute(0, 2, 1, 3, 5, 4).contiguous().view(B*self.last_size**2, self.pred_step, B*self.last_size**2, N)
            for j in range(B*self.last_size**2):
                tmp[j, torch.arange(self.pred_step), j, torch.arange(N-self.pred_step, N)] = 1 # pos
            mask = tmp.view(B, self.last_size**2, self.pred_step, B, self.last_size**2, N).permute(0,2,1,3,5,4)
            self.mask = mask

        return [score, self.mask]

    def _initialize_weights(self, module):
        for name, param in module.named_parameters():
            if 'bias' in name:
                nn.init.constant_(param, 0.0)
            elif 'weight' in name:
                nn.init.orthogonal_(param, 1)
        # other resnet weights have been initialized in resnet itself

    def reset_mask(self):
        self.mask = None

# class TE_DPC_RNN(nn.Module):
#     '''DPC with RNN, for use in the TE supermodule'''
#     def __init__(self, sample_size, num_seq=8, seq_len=1, num_aggregation_steps=1, pred_step=2, network='resnet18_2d'):
#         super(TE_DPC_RNN, self).__init__()

#         self.sample_size = sample_size
#         self.num_seq = num_seq
#         self.seq_len = seq_len
#         self.num_aggregation_steps = num_aggregation_steps
#         self.pred_step = pred_step
#         assert num_seq-num_aggregation_steps >= pred_step
#         self.last_duration = int(math.ceil(seq_len / 4))
#         self.last_size = int(math.ceil(sample_size / 32))
#         print('final feature map has size %dx%d' % (self.last_size, self.last_size))

#         self.backbone, self.param = select_resnet(network, track_running_stats=False)
#         self.param['num_layers'] = 1 # param for GRU
#         self.param['hidden_size'] = self.param['feature_size'] # param for GRU

#         self.agg = ConvGRU(input_size=self.param['feature_size'],
#                                hidden_size=self.param['hidden_size'],
#                                kernel_size=1,
#                                num_layers=self.param['num_layers'],return_every_layer = True)
#         self.network_pred = nn.Sequential(
#                                 nn.Conv2d(self.param['feature_size'], self.param['feature_size'], kernel_size=1, padding=0),
#                                 nn.ReLU(inplace=True),
#                                 nn.Conv2d(self.param['feature_size'], self.param['feature_size'], kernel_size=1, padding=0)
#                                 )
#         self.mask = None
#         self.relu = nn.ReLU(inplace=False)
#         self.criterion = nn.CrossEntropyLoss()
#         self._initialize_weights(self.agg)
#         self._initialize_weights(self.network_pred)

#     def forward(self, block, calculate_loss = False):
#         # block: [B, N, C, SL, W, H]
#         ### extract feature ###
#         if len(block.shape) == 6:
#             (B, N, C, SL, H, W) = block.shape
#             assert SL==1, 'sequence length should be 1 since data is 2d'
#         elif len(block.shape) == 5:
#             (B, N, C, 1, H, W) = block.unsqueeze(3).shape
#             SL = 1
#         else:
#             raise f"input shape {block.shape} should be length 5 or 6"

#         # block = block.view(B*N, C, SL, H, W)
#         block = block.view(B*N, C, H, W) # for 2d resnet
#         feature = self.backbone(block)
#         del block
#         feature = F.avg_pool3d(feature, (self.last_duration, 1, 1), stride=(1, 1, 1))

#         feature_inf_all = feature.view(B, N, self.param['feature_size'], self.last_size, self.last_size) # before ReLU, (-inf, +inf)
#         # feature = self.relu(feature) # [0, +inf) #[0, +inf). Don't want relu here? commenting this out makes feature_inf_all==feature
#         feature = feature.view(B, N, self.param['feature_size'], self.last_size, self.last_size) # [B,N,D,6,6]
#         z_seq = feature


#         ### aggregate, predict future ###
#         full_hidden, last_hidden = self.agg(feature.contiguous()) # full_hidden is shape [B,N,num_layers,hidden_size,H,W]
#         c_seq = full_hidden[:,:,-1,...] # last layers
        
#         # loop over possible start points for prediction
#         first_preds_list = []
#         scores_list = []
#         # total_score = 0
#         for l in range(self.num_aggregation_steps, N-1):
#             hidden = c_seq[:,l,...]
#             pred = []
#             for i in range(min(self.pred_step,N-l-1)):
#                 # sequentially pred future
#                 p_tmp = self.network_pred(hidden)
#                 pred.append(p_tmp)
#                 if i == 0:
#                     first_preds_list.append(pred)
                
#                 # only need further predictions for the scores
#                 if calculate_loss:
#                     _, hidden = self.agg(self.relu(p_tmp).unsqueeze(1), hidden.unsqueeze(0)) # for convgru input is batch first but hidden is not
#                     hidden = hidden[:,-1,...]
#                 else:
#                     break
#             pred = torch.stack(pred, 1) # B, pred_step, xxx
#             del hidden

#             ### loss calculations ###
#             if calculate_loss:
#                 ### Get similarity score ###
#                 _N = min(self.pred_step,N-l-1) # prediction length
#                 feature_inf = feature_inf_all[:, l+1:l+1+_N,...].contiguous()
#                 # pred: [B, pred_step, D, last_size, last_size]
#                 # GT: [B, N, D, last_size, last_size]

#                 # dot product D dimension in pred-GT pair, get a 6d tensor. First 3 dims are from pred, last 3 dims are from GT. 
#                 pred = pred.permute(0,1,3,4,2).contiguous().view(B*_N*self.last_size**2, self.param['feature_size'])
#                 feature_inf = feature_inf.permute(0,1,3,4,2).contiguous().view(B*_N*self.last_size**2, self.param['feature_size']).transpose(0,1)
#                 score = torch.matmul(pred, feature_inf).view(B, self.pred_step, self.last_size**2, B, _N, self.last_size**2)
#                 del feature_inf, pred

#                 scores_list.append(score)

#                 if self.mask is None or self.pred_step > N-l-1: # only compute mask as needed
#                     #TODO left here
#                     # mask meaning: -2: omit, -1: temporal neg (hard), 0: easy neg, 1: pos, -3: spatial neg
#                     mask = torch.zeros((B, _N, self.last_size**2, B, _N, self.last_size**2), dtype=torch.int8, requires_grad=False).detach().to(device=z_seq.device)
#                     mask[torch.arange(B), :, :, torch.arange(B), :, :] = -3 # spatial neg
#                     for k in range(B):
#                         mask[k, :, torch.arange(self.last_size**2), k, :, torch.arange(self.last_size**2)] = -1 # temporal neg
#                     tmp = mask.permute(0, 2, 1, 3, 5, 4).contiguous().view(B*self.last_size**2, self.pred_step, B*self.last_size**2, _N)
#                     for j in range(B*self.last_size**2):
#                         tmp[j, torch.arange(self.pred_step), j, torch.arange(_N-self.pred_step, _N)] = 1 # pos
#                     mask = tmp.view(B, self.last_size**2, self.pred_step, B, self.last_size**2, _N).permute(0,2,1,3,5,4)
#                     self.mask = mask

#                     (_, NP, SQ, B2, NS, _) = mask.size() # [B, P, SQ, B2, N, SQ]
#                     target_ = mask == 1
#                     target_.requires_grad = False


            

#                 # score is a 6d tensor: [B, P, SQ, B2, N, SQ]
#                 # similarity matrix is computed inside each gpu, thus here B == num_gpu * B2
#                 score_flattened = score_.view(B*NP*SQ, B2*NS*SQ)
#                 target_flattened = target_.view(B*NP*SQ, B2*NS*SQ)
#                 target_flattened = target_flattened.to(int).argmax(dim=1)

#                 loss = self.criterion(score_flattened, target_flattened)
#                 top1, top3, top5 = calc_topk_accuracy(score_flattened, target_flattened, (1,3,5))

#                 accuracy_list[0].update(top1.item(),  B)
#                 accuracy_list[1].update(top3.item(), B)
#                 accuracy_list[2].update(top5.item(), B)

#                 losses.update(loss.item(), B)
#                 accuracy.update(top1.item(), B)
        
#         zhat_seq = torch.stack(first_preds_list, dim=1) # B,N-1,D,6,6

#         if calculate_loss:
#             return z_seq, c_seq, zhat_seq, [scores_list, self.mask]
#         else:
#             return z_seq, c_seq, zhat_seq, [None, None, None]

#     def _initialize_weights(self, module):
#         for name, param in module.named_parameters():
#             if 'bias' in name:
#                 nn.init.constant_(param, 0.0)
#             elif 'weight' in name:
#                 nn.init.orthogonal_(param, 1)
#         # other resnet weights have been initialized in resnet itself

#     def reset_mask(self):
#         self.mask = None

class TE_DPC_RNN(nn.Module):
    '''DPC with RNN, for use in the TE supermodule'''
    def __init__(self, sample_size=(128,128), num_seq=8, seq_len=1, num_aggregation_steps=1, pred_step=2, network='resnet18_2d'):
        super(TE_DPC_RNN, self).__init__()

        self.sample_size = sample_size
        self.num_seq = num_seq
        self.seq_len = seq_len
        self.num_aggregation_steps = num_aggregation_steps
        self.pred_step = pred_step
        assert num_seq-num_aggregation_steps >= pred_step
        self.last_duration = int(math.ceil(seq_len / 4))
        self.last_size = (int(math.ceil(sample_size / 32)),int(math.ceil(sample_size / 32)))
        print('final feature map has size %dx%d' % (self.last_size[0], self.last_size[1]))

        self.backbone, self.param = select_resnet(network, track_running_stats=False)
        self.param['num_layers'] = 1 # param for GRU
        self.param['hidden_size'] = self.param['feature_size'] # param for GRU

        self.agg = ConvGRU(input_size=self.param['feature_size'],
                               hidden_size=self.param['hidden_size'],
                               kernel_size=1,
                               num_layers=self.param['num_layers'],return_every_layer = True)
        self.network_pred = nn.Sequential(
                                nn.Conv2d(self.param['feature_size'], self.param['feature_size'], kernel_size=1, padding=0),
                                nn.ReLU(inplace=True),
                                nn.Conv2d(self.param['feature_size'], self.param['feature_size'], kernel_size=1, padding=0)
                                )
        self.mask = None
        self.relu = nn.ReLU(inplace=False)
        self.criterion = nn.CrossEntropyLoss()
        self._initialize_weights(self.agg)
        self._initialize_weights(self.network_pred)

    def forward(self, block, calculate_score = True, just_cpc = False):
        # for training cpc one would have calculate_score = True, otherwise if you just want z_seq, c_seq , and zhat_seq, set to false to save compute
        # If we are just training cpc and do not require returning z_seq, c_seq , and zhat_seq, then set just_cpc = True to save compute
        # block: [B, N, C, SL, W, H]
        if just_cpc:
            assert calculate_score, "need to calculate score for cpc"
        ### extract feature ###
        if len(block.shape) == 6:
            (B, N, C, SL, H, W) = block.shape
            assert SL==1, 'sequence length should be 1 since data is 2d'
        elif len(block.shape) == 5:
            (B, N, C, SL, H, W) = block.unsqueeze(3).shape
            # SL = 1
        else:
            raise f"input shape {block.shape} should be length 5 or 6"


        # block = block.view(B*N, C, SL, H, W)
        block = block.view(B*N, C, H, W) # for 2d resnet
        feature = self.backbone(block)
        del block
        feature = F.avg_pool3d(feature, (self.last_duration, 1, 1), stride=(1, 1, 1))

        feature_inf_all = feature.view(B, N, self.param['feature_size'], self.last_size[0], self.last_size[1]) # before ReLU, (-inf, +inf)
        # feature = self.relu(feature) # [0, +inf) #[0, +inf). Don't want relu here? commenting this out makes feature_inf_all==feature
        feature = feature.view(B, N, self.param['feature_size'], self.last_size[0], self.last_size[1]) # [B,N,D,6,6]
        z_seq = feature


        # random start point after agg steps for cpc
        if calculate_score:
            cpc_start = self.num_aggregation_steps + random.randint(self.num_aggregation_steps,N-1 - self.pred_step)
            feature_inf = feature_inf_all[:, cpc_start:cpc_start + self.pred_step, :].contiguous()
        else:
            # no cpc
            cpc_start = N


        ### aggregate, predict future ###
        if just_cpc:
            full_hidden, last_hidden = self.agg(feature[:,:cpc_start + 1,...].contiguous()) # full_hidden is shape [B,cpc_start +1,num_layers,hidden_size,H,W]
            c_seq = full_hidden[:,:,-1,...] # last layers
            outer_start = cpc_start
            outer_steps = cpc_start+1
        else:
            full_hidden, last_hidden = self.agg(feature.contiguous()) # full_hidden is shape [B,N,num_layers,hidden_size,H,W]
            c_seq = full_hidden[:,:,-1,...] # last layers
            outer_start = self.num_aggregation_steps
            outer_steps = N-1
        
        # loop over possible start points for prediction
        first_preds_list = []
        for l in range(outer_start, outer_steps):
            hidden = c_seq[:,l,...]
            pred = []
            for i in range(min(self.pred_step,N-l-1)):
                # sequentially pred future
                p_tmp = self.network_pred(hidden)
                if i == 0:
                    first_preds_list.append(p_tmp)
                
                # only need further predictions for the score for cpc
                if cpc_start<=l+i and l+i<self.pred_step:
                    pred.append(p_tmp)
                    _, hidden = self.agg(self.relu(p_tmp).unsqueeze(1), hidden.unsqueeze(0)) # for convgru input is batch first but hidden is not
                    hidden = hidden[:,-1,...]
                else:
                    break
            if calculate_score:
                pred = torch.stack(pred, 1) # B, pred_step, xxx
            del hidden

        
        score = None
        if calculate_score:
            ### Get similarity score ###
            _N = self.pred_step # prediction length
            # pred: [B, pred_step, D, last_size[0], last_size[1]]
            # GT: [B, N, D, last_size[0], last_size[1]]

            # dot product D dimension in pred-GT pair, get a 6d tensor. First 3 dims are from pred, last 3 dims are from GT. 
            pred = pred.permute(0,1,3,4,2).contiguous().view(B*_N*self.last_size[0]*self.last_size[1], self.param['feature_size'])
            feature_inf = feature_inf.permute(0,1,3,4,2).contiguous().view(B*_N*self.last_size[0]*self.last_size[1], self.param['feature_size']).transpose(0,1)
            score = torch.matmul(pred, feature_inf).view(B, self.pred_step, self.last_size[0]*self.last_size[1], B, _N, self.last_size[0]*self.last_size[1])
            del feature_inf, pred

            if self.mask is None and calculate_score: # only compute mask as needed
                #TODO left here
                # mask meaning: -2: omit, -1: temporal neg (hard), 0: easy neg, 1: pos, -3: spatial neg
                mask = torch.zeros((B, _N, self.last_size[0]*self.last_size[1], B, _N, self.last_size[0]*self.last_size[1]), dtype=torch.int8, requires_grad=False).detach().to(device=z_seq.device)
                mask[torch.arange(B), :, :, torch.arange(B), :, :] = -3 # spatial neg
                for k in range(B):
                    mask[k, :, torch.arange(self.last_size[0]*self.last_size[1]), k, :, torch.arange(self.last_size[0]*self.last_size[1])] = -1 # temporal neg
                tmp = mask.permute(0, 2, 1, 3, 5, 4).contiguous().view(B*self.last_size[0]*self.last_size[1], self.pred_step, B*self.last_size[0]*self.last_size[1], _N)
                for j in range(B*self.last_size[0]*self.last_size[1]):
                    tmp[j, torch.arange(self.pred_step), j, torch.arange(_N-self.pred_step, _N)] = 1 # pos
                mask = tmp.view(B, self.last_size[0]*self.last_size[1], self.pred_step, B, self.last_size[0]*self.last_size[1], _N).permute(0,2,1,3,5,4)
                self.mask = mask

                    
        if just_cpc:
            zhat_seq = None
            c_seq = None
        else:
            zhat_seq = torch.stack(first_preds_list, dim=1) # B,N-1,D,6,6

        
        return z_seq, c_seq, zhat_seq, [score, self.mask]

    @abstractmethod
    def calc_topk_accuracy(output, target, topk=(1,)):
        '''
        Modified from: https://gist.github.com/agermanidis/275b23ad7a10ee89adccf021536bb97e
        Given predicted and ground truth labels, 
        calculate top-k accuracies.
        '''
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].contiguous().view(-1).float().sum(0)
            res.append(correct_k.mul_(1 / batch_size))
        return res

    @abstractmethod
    def process_mask(mask):
        '''task mask as input, compute the target for contrastive loss'''
        # dot product is computed in parallel gpus, so get less easy neg, bounded by batch size in each gpu'''
        # mask meaning: -2: omit, -1: temporal neg (hard), 0: easy neg, 1: pos, -3: spatial neg
        (B, NP, SQ, B2, NS, _) = mask.size() # [B, P, SQ, B, N, SQ]
        target = mask == 1
        target.requires_grad = False
        return target, (B, B2, NS, NP, SQ)

    def contrastive_loss(self, mask, score):
        # not to be used in parallel

        if self.processed_target is None:
            self.processed_target = self.process_mask(mask)

        target_, (B, B2, NS, NP, SQ) = self.processed_target

        # score is a 6d tensor: [B, P, SQ, B2, N, SQ]
        # similarity matrix is computed inside each gpu, thus here B == num_gpu * B2
        score_flattened = score.view(B*NP*SQ, B2*NS*SQ)
        target_flattened = target_.view(B*NP*SQ, B2*NS*SQ)
        target_flattened = target_flattened.to(int).argmax(dim=1)

        loss = self.criterion(score_flattened, target_flattened)
        top1, top3, top5 = self.calc_topk_accuracy(score_flattened, target_flattened, (1,3,5))

        return loss, top1, top3, top5

    def _initialize_weights(self, module):
        for name, param in module.named_parameters():
            if 'bias' in name:
                nn.init.constant_(param, 0.0)
            elif 'weight' in name:
                nn.init.orthogonal_(param, 1)
        # other resnet weights have been initialized in resnet itself

    def reset_mask(self):
        self.mask = None

    def reset_processed_target(self):
        self.processed_target = None

